# Travail pratique 1

## Description

Ce projet consiste à créer un logiciel qui prendra en entrée une suite de nombres représentants des hauteurs et qui affiche sur la sortie standard des chaînes de caractères représentant une montagne dont la forme est déterminée par ces hauteurs. Il remplira ensuite les portions concaves avec un charactère représentant l'eau qui pourrait s'y accumuler.

Ce projet est fait dans le cadre du cours INF3135 à l'UQAM à des fins éducatives.

## Auteur

Pascal Vautour (VAUP05049304)

## Fonctionnement

Pour faire fonctionner le programme, il faut l'appeler de la ligne de commant en y inscrivant les elements suivants separes d'espaces:

    1. ./tp1
    2. Un caractere unique au choix
    3. Un caractere unique au choix different du premier
    4. Un a 20 nombres separes de virgules, tous entre 0 et 15 inclusivement.

Un exemple d'utilisation valide:

Entree:

./tp1 X E 1,2,4,2,5,1,2,3,1,5,3

Affichage a la console:

----XEEEEX-
--XEXEEEEX-
--XEXEEXEXX
-XXXXEXXEXX
XXXXXXXXXXX

Le programme gerera la majorite des cas d'entree invalide par un arret complet et le retour a la console d'un message d'erreur approprie.

Un exemple dans le cas d'une entree invalide:

Entree:

 ./tp1 X 1,2,1

Sortie:

Nombre d'arguments invalides: il en faut 3

## Contenu du projet

Il y à présentement dans le projet 4 fichiers:

	*README.md, qui sert à donner des informations générales quant au projet, son état et son utilisation.
	*tp1.c, qui devrait contenir l'entièreté du code du projet.
	*test.bats, qui contient les tests pour vérifier que le logiciel remplis les contraintes requises.
	*Makefile, qui supporte les appels make et make clean.

## Références

Aucune source à l'exterieur de ce qui etait fourni dans le cadre du cours inf3135 à l'UQAM n'a ete utilisee.

## Statut

Le programme est complete sans bogue. Cependant, il contient presentement la/les limite(s) suivante(s):

    *si un utilisateur souhaite augmenter la constante LARGEUR_MAX, il devra faire attention de s'assurer que la variable tmp de la fonction initTabHauteurs ait assez de memoire a sa disposition manuellement.	


